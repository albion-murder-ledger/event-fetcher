package main

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"github.com/joho/godotenv"
)

var db *sql.DB

func main() {
	var err error

	scanPointer := 367546386

	err = godotenv.Load()
	if err != nil {
		log.Fatal(err)
	}

	db, err := sqlx.Open("mysql", os.Getenv("DB_URL"))
	if err != nil {
		panic(err)
	}

	db.SetConnMaxLifetime(time.Minute * 3)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)

	log.Println("Connected to DB")

	fmt.Printf("gap size, gap seconds, last id, next id, last time, next time, event timestamp\n")
	for {
		newPointer := fetchBatch(db, scanPointer, 10000)
		if newPointer == scanPointer {
			break
		}

		scanPointer = newPointer
	}
}

func fetchBatch(db *sqlx.DB, pointer int, size int) int {
	rows, err := db.Queryx("select event_id, UNIX_TIMESTAMP(time) from events where event_id > ? order by event_id asc limit ?", pointer, size)
	if err != nil {
		log.Fatal(err)
	}

	var last = pointer
	var last_time time.Time
	var curr int
	var curr_timestamp int64

	for rows.Next() {
		err = rows.Scan(&curr, &curr_timestamp)
		if err != nil {
			log.Println(err)
		}

		curr_time := time.Unix(curr_timestamp, 0)
		diff := curr - last
		duration := curr_time.Sub(last_time)

		if diff > 10 && duration.Seconds() > 5.0 && duration.Seconds() < 1000000.0 && curr_time.Hour() != 4 && last_time.Hour() != 4 {
			fmt.Printf("%d, %.0f, %d, %d, %s, %s, %d\n", diff, duration.Seconds(), last, curr, last_time.Format(time.RFC3339), curr_time.Format(time.RFC3339), last_time.Unix())
		}

		last = curr
		last_time = curr_time
	}

	return last
}
