package rnkr

type ByRating []*Rating

func (a ByRating) Len() int {
	return len(a)
}

func (a ByRating) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}

func (a ByRating) Less(i, j int) bool {
	return a[i].Rating() < a[j].Rating()
}
