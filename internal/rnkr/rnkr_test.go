package rnkr

import (
	"testing"
)

func TestFresh1v1_WinnerSkillIncreasesLoserSkillDecreases(t *testing.T) {
	// Arrange
	winnerRating := DefaultRating()
	loserRating := DefaultRating()

	teams := [][]*Rating{
		{winnerRating},
		{loserRating},
	}
	rater := DefaultRater()

	// Act
	newRanks, err := rater.UpdateRatings(teams, []uint{1, 2})
	winnerNewRating := newRanks[0][0]
	loserNewRating := newRanks[1][0]

	if err != nil {
		t.Fatalf("Got error while updating ratings: %v", err)
	}

	// Assert
	if !(winnerNewRating.EstimatedSkill > winnerRating.EstimatedSkill) {
		t.Fatalf("Expected winner skill to increase (%f > %f)", winnerNewRating.EstimatedSkill, winnerRating.EstimatedSkill)
	}

	if !(loserNewRating.EstimatedSkill < loserRating.EstimatedSkill) {
		t.Fatalf("Expected loser skill to decrease (%f < %f)", loserNewRating.EstimatedSkill, loserRating.EstimatedSkill)
	}

	if !(winnerNewRating.Uncertainty < winnerRating.Uncertainty) {
		t.Fatalf("Expected winner uncertainty to decrease (%f < %f)", winnerNewRating.Uncertainty, winnerRating.Uncertainty)
	}

	if !(loserNewRating.Uncertainty < loserRating.Uncertainty) {
		t.Fatalf("Expected loser uncertainty to decrease (%f < %f)", loserNewRating.Uncertainty, loserRating.Uncertainty)
	}
}

func TestUpdateRatings_UpsetShouldTransferMoreRating(t *testing.T) {
	// Arrange
	underdog := NewRating("underdog", 25, 5)
	champ := NewRating("champ", 45, 5)

	fairWinner := NewRating("winner", 25, 5)
	fairLoser := NewRating("loser", 25, 5)

	rater := DefaultRater()

	// Act
	upsetResult, err := rater.UpdateRatings([][]*Rating{{underdog}, {champ}}, []uint{1, 2})
	if err != nil {
		t.Fatalf("Got error while updating ratings: %v", err)
	}
	upsetWinnerChange := upsetResult[0][0].EstimatedSkill - underdog.EstimatedSkill
	upsetLoserChange := upsetResult[1][0].EstimatedSkill - champ.EstimatedSkill

	fairResult, err := rater.UpdateRatings([][]*Rating{{fairWinner}, {fairLoser}}, []uint{1, 2})
	if err != nil {
		t.Fatalf("Got error while updating ratings: %v", err)
	}
	fairWinnerChange := fairResult[0][0].EstimatedSkill - fairWinner.EstimatedSkill
	fairLoserChange := fairResult[1][0].EstimatedSkill - fairLoser.EstimatedSkill

	// Assert
	if !(upsetWinnerChange > fairWinnerChange) {
		t.Fatalf("Expected underdog to gain more points from an upset, than a fair match %f > %f", upsetWinnerChange, fairWinnerChange)
	}

	if !(fairLoserChange > upsetLoserChange) {
		t.Fatalf("Expected champ to lose more points from an upset, than a fair match %f > %f", fairLoserChange, upsetLoserChange)
	}
}

func TestUpdateRatings_ExpectedOutcomeShouldTransferLessRating(t *testing.T) {
	// Arrange
	underdog := NewRating("underdog", 25, 5)
	champ := NewRating("champ", 45, 5)

	fairWinner := NewRating("winner", 25, 5)
	fairLoser := NewRating("loser", 25, 5)

	rater := DefaultRater()

	// Act
	expectedResult, err := rater.UpdateRatings([][]*Rating{{champ}, {underdog}}, []uint{1, 2})
	if err != nil {
		t.Fatalf("Got error while updating ratings: %v", err)
	}
	expectedWinnerChange := expectedResult[0][0].EstimatedSkill - champ.EstimatedSkill
	expectedLoserChange := expectedResult[1][0].EstimatedSkill - underdog.EstimatedSkill

	fairResult, err := rater.UpdateRatings([][]*Rating{{fairWinner}, {fairLoser}}, []uint{1, 2})
	if err != nil {
		t.Fatalf("Got error while updating ratings: %v", err)
	}
	fairWinnerChange := fairResult[0][0].EstimatedSkill - fairWinner.EstimatedSkill
	fairLoserChange := fairResult[1][0].EstimatedSkill - fairLoser.EstimatedSkill

	// Assert
	if !(fairWinnerChange > expectedWinnerChange) {
		t.Fatalf("Expected the champ to gain fewer points from an uneven win, than a fair match %f > %f", fairWinnerChange, expectedWinnerChange)
	}

	if !(expectedLoserChange > fairLoserChange) {
		t.Fatalf("Expected underdog to lose fewer points from an uneven match, than a fair match %f > %f", expectedLoserChange, fairLoserChange)
	}
}

func TestUpdateRatings_Fair2v2_ExpectWinnersGainLoserLose(t *testing.T) {
	// Arrange
	a1 := NewRating("p1", 25, 5)
	a2 := NewRating("p2", 25, 5)

	b1 := NewRating("p3", 25, 5)
	b2 := NewRating("p4", 25, 5)

	rater := DefaultRater()

	// Act
	result, err := rater.UpdateRatings([][]*Rating{{a1, a2}, {b1, b2}}, []uint{1, 2})

	// Assert
	if err != nil {
		t.Fatalf("Got error while updating ratings: %v", err)
	}

	if !(result[0][0].EstimatedSkill > a1.EstimatedSkill) {
		t.Fatalf("Expected winner a1 estimated skill to increase: %f > %f", result[0][0].EstimatedSkill, a1.EstimatedSkill)
	}
	if !(result[0][1].EstimatedSkill > a2.EstimatedSkill) {
		t.Fatalf("Expected winner a2 estimated skill to increase: %f > %f", result[0][1].EstimatedSkill, a1.EstimatedSkill)
	}
	if !(result[1][0].EstimatedSkill < b1.EstimatedSkill) {
		t.Fatalf("Expected loser b1 estimated skill to decrease: %f < %f", result[1][0].EstimatedSkill, b1.EstimatedSkill)
	}
	if !(result[1][1].EstimatedSkill < b2.EstimatedSkill) {
		t.Fatalf("Expected loser b2 estimated skill to decrease: %f < %f", result[1][1].EstimatedSkill, b2.EstimatedSkill)
	}
}

func TestUpdateRatings_BalancedVsUnbalanced2v2_ExpectNewerWinnerToGainMoreThanVeteranWinner(t *testing.T) {
	// Arrange
	a1 := NewRating("p1", 5, 8)
	a2 := NewRating("p2", 45, 2)

	b1 := NewRating("p3", 25, 5)
	b2 := NewRating("p4", 25, 5)

	rater := DefaultRater()

	// Act
	result, err := rater.UpdateRatings([][]*Rating{{a1, a2}, {b1, b2}}, []uint{1, 2})
	a1Change := result[0][0].EstimatedSkill - a1.EstimatedSkill
	a2Change := result[0][1].EstimatedSkill - a2.EstimatedSkill

	// Assert
	if err != nil {
		t.Fatalf("Got error while updating ratings: %v", err)
	}

	if !(a1Change > a2Change) {
		t.Fatalf("Expected lower rank winner to gain more than high rank winner: %f > %f", a1Change, a2Change)
	}
}

func TestUpdateRatings_CanUncertaintyIncrase(t *testing.T) {
	// Arrange
	p1 := NewRating("p1", 5, 2)
	p2 := NewRating("p2", 45, 1)
	rater := DefaultRater()

	// Act
	result, err := rater.UpdateRatings([][]*Rating{{p1}, {p2}}, []uint{1, 2})

	// Assert
	if err != nil {
		t.Fatalf("Got error while updating ratings: %v", err)
	}

	if !(result[0][0].Uncertainty < p1.Uncertainty) {
		t.Fatalf("Expected uncertainty to decrease after massive upset????")
	}
}
