package elo

import "math"

const (
	p = 0.5 // Power diff scale factor
)

func winnerProb(winnerRating, loserRating, winnerIp, loserIp int) float64 {
	powerDiffPercent := (float64(winnerIp - loserIp)) / 10 / 100
	ratingDiff := float64(loserRating) - (float64(winnerRating) * (1 + powerDiffPercent*p))

	return 1 / (1 + math.Pow(10, (ratingDiff)/400))
}

func CalcPoints(winnerRating, loserRating, winnerIp, loserIp int) (int, int) {
	prob := winnerProb(winnerRating, loserRating, winnerIp, loserIp)

	winnerK := getKValue(winnerRating)
	loserK := getKValue(loserRating)

	winnerPoints := int(math.Max(1, math.Round(winnerK*(1.0-prob))))
	loserPoints := int(math.Max(1, math.Round(loserK*(1.0-prob))))

	return winnerPoints, loserPoints
}

func getKValue(rating int) float64 {
	if rating < 2100 {
		return 32
	} else if rating < 2400 {
		return 24
	} else {
		return 16
	}
}
